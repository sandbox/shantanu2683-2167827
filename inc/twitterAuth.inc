<?php

/**
 * @file
 * Twitter-API-PHP : Simple PHP wrapper for the v1.1 API
 * 
 * PHP version 5.3.10
 * 
 * @category Awesomeness
 * @package  Twitter-API-PHP
 * @license  MIT License
 * @link     http://github.com/j7mbo/twitter-api-php
 */

class TwitterFeedBlockTwitterAPIExchange {

  protected $oauthAccessToken;
  protected $oauthAccessTokenSecret;
  protected $consumerKey;
  protected $consumerSecret;
  protected $postfields;
  protected $getfield;
  protected $oauth;
  public $url;

  /**
   * Create the API access object. Requires an array of settings.
   */
  public function __construct(array $settings) {
    if (!in_array('curl', get_loaded_extensions())) {
      throw new Exception('You need to install cURL, see: http://curl.haxx.se/docs/install.html');
    }

    if (!isset($settings['oauth_access_token'])
            || !isset($settings['oauth_access_token_secret'])
            || !isset($settings['consumer_key'])
            || !isset($settings['consumer_secret'])) {
      throw new Exception('Make sure you are passing in the correct parameters');
    }

    $this->oauthAccessToken = $settings['oauth_access_token'];
    $this->oauthAccessTokenSecret = $settings['oauth_access_token_secret'];
    $this->consumerKey = $settings['consumer_key'];
    $this->consumerSecret = $settings['consumer_secret'];
  }

  /**
   * Set postfields array, example: array('screen_name' => 'J7mbo').
   */
  public function setPostfields(array $array) {
    if (!is_null($this->getGetfield())) {
      throw new Exception('You can only choose get OR post fields.');
    }

    if (isset($array['status']) && substr($array['status'], 0, 1) === '@') {
      $array['status'] = sprintf("\0%s", $array['status']);
    }

    $this->postfields = $array;

    return $this;
  }

  /**
   * Set getfield string, example: '?screen_name=J7mbo'.
   */
  public function setGetfield($string) {
    if (!is_null($this->getPostfields())) {
      throw new Exception('You can only choose get OR post fields.');
    }

    $search = array('#', ',', '+', ':');
    $replace = array('%23', '%2C', '%2B', '%3A');
    $string = str_replace($search, $replace, $string);

    $this->getfield = $string;

    return $this;
  }

  /**
   * Get getfield string (simple getter).
   * 
   * @return string
   *   $this->getfields
   */
  public function getGetfield() {
    return $this->getfield;
  }

  /**
   * Get postfields array (simple getter).
   * 
   * @return array
   *   $this->postfields
   */
  public function getPostfields() {
    return $this->postfields;
  }

  /**
   * Build the Oauth object using params set in construct and additionals.
   */
  public function buildOauth($url, $request_method) {
    if (!in_array(strtolower($request_method), array('post', 'get'))) {
      throw new Exception('Request method must be either POST or GET');
    }

    $consumer_key = $this->consumerKey;
    $consumer_secret = $this->consumerSecret;
    $oauth_access_token = $this->oauthAccessToken;
    $oauth_access_token_secret = $this->oauthAccessTokenSecret;

    $oauth = array(
      'oauth_consumer_key' => $consumer_key,
      'oauth_nonce' => time(),
      'oauth_signature_method' => 'HMAC-SHA1',
      'oauth_token' => $oauth_access_token,
      'oauth_timestamp' => time(),
      'oauth_version' => '1.0',
    );

    $getfield = $this->getGetfield();

    if (!is_null($getfield)) {
      $getfields = str_replace('?', '', explode('&', $getfield));
      foreach ($getfields as $g) {
        $split = explode('=', $g);
        $oauth[$split[0]] = $split[1];
      }
    }

    $base_info = $this->buildBaseString($url, $request_method, $oauth);
    $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
    $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, TRUE));
    $oauth['oauth_signature'] = $oauth_signature;

    $this->url = $url;
    $this->oauth = $oauth;

    return $this;
  }

  /**
   * Perform the actual data retrieval from the API.
   * 
   * @param bool $return 
   *   If true, returns data.
   * 
   * @return string
   *   json If $return param is true, returns json data.
   */
  public function performRequest($return = TRUE) {
    if (!is_bool($return)) {
      throw new Exception('performRequest parameter must be true or false');
    }

    $header = array($this->buildAuthorizationHeader($this->oauth), 'Expect:');

    $getfield = $this->getGetfield();
    $postfields = $this->getPostfields();

    $options = array(
      CURLOPT_HTTPHEADER => $header,
      CURLOPT_HEADER => FALSE,
      CURLOPT_URL => $this->url,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_TIMEOUT => 10,
    );

    if (!is_null($postfields)) {
      $options[CURLOPT_POSTFIELDS] = $postfields;
    }
    else {
      if ($getfield !== '') {
        $options[CURLOPT_URL] .= $getfield;
      }
    }

    $feed = curl_init();
    curl_setopt_array($feed, $options);
    $json = curl_exec($feed);
    curl_close($feed);

    if ($return) {
      return $json;
    }
  }

  /**
   * Private method to generate the base string used by cURL.
   * 
   * @param string $base_uri
   *   base url
   * @param string $method
   *   which method
   * @param array $params
   *   list of params
   * 
   * @return string
   *   Built base string returned.
   */
  protected function buildBaseString($base_uri, $method, $params) {
    $return = array();
    ksort($params);

    foreach ($params as $key => $value) {
      $return[] = "$key=" . $value;
    }

    return $method . "&" . rawurlencode($base_uri) . '&' . rawurlencode(implode('&', $return));
  }

  /**
   * Private method to generate authorization header used by cURL.
   *
   * @param array $oauth 
   *   Array of oauth data generated by buildOauth()
   */
  protected function buildAuthorizationHeader($oauth) {
    $return = 'Authorization: OAuth ';
    $values = array();

    foreach ($oauth as $key => $value) {
      $values[] = "$key=\"" . rawurlencode($value) . "\"";
    }

    $return .= implode(', ', $values);
    return $return;
  }

}
