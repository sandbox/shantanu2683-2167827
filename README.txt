-- SUMMARY --

Twitter 1.1 version.

he Twitter REST API v1 is no longer active. Please migrate to API v1.1

If you are very new on twitter login to your account and visit this url
https://dev.twitter.com/apps/new
Create your first application.

For Call back url you no need to put any thing.


-- REQUIREMENTS --

None.

INSTALLATION
------------

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure user permissions in Administration � People � Permissions:
* Set your twitter api setting detail here Administration � Configuration
    � Web services � Twitter Oauth
* You can see two block in Administration � Structure � Blocks
    1.Tweet feed search block == This block is for showing search value
    as you search in twitter.For more detail you can see this link
    https://dev.twitter.com/docs/api/1.1/get/search/tweets
    2.Tweet feed user block == By using this block you can show any
    twitter user's feed.
* There is option to set number of feed you like to show in a block.
    


CREDITS
-------
7.x Developed & Maintained by
  Shantanu Karmakar (shantanu2683) https://drupal.org/user/728870
  
