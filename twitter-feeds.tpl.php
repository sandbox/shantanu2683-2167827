<?php

/**
 * @file
 * Default theme implementation to present all twitter feed data.
 */
 ?>
<div class="tweet-val">
  <div class="tweet-val-photo">
    <img src="<?php print $photo; ?>" alt="<?php print $name; ?>">
  </div>
  <div class="tweet-val-text">
    <?php print $text; ?>
  <div class="tweet-val-text-clear"></div>
  </div>  
</div>
